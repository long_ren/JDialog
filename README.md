JDialog
========
>
开发：yangjian<br />
文档编写：yangjian<br />
Email : yangjian102621@163.com

插件描述:
--------
JDialog是一个js编写的对话框插件。包括：
* JDialog.lock 锁屏插件
* JDialog.tip 提示框         
* JDialog.win 对话框|确认框窗口

插件依赖:
-------
* jQuery-1.7.1以上版本
* 需要引进JDialog.css文件以及一些提示图标



